package cn.yunhe.springmvc.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@Controller
public class DownAction {

	@RequestMapping(value = "/index")
	public String index() {
		return "index";
	}

	// SPRINGMVC中支持的下载
	@RequestMapping("/downfile2")
	public ResponseEntity<byte[]> download() throws IOException {
		File file = new File("G:\\java\\eclipse_workspace\\yunhe\\springmvc\\file_updown_03\\src\\main\\webapp\\upload\\1.png");
		// 处理显示中文文件名的问题
		String fileName = new String(file.getName().getBytes("utf-8"), "ISO-8859-1");
		// 设置请求头内容,告诉浏览器代开下载窗口
		HttpHeaders headers = new HttpHeaders();
		headers.setContentDispositionFormData("attachment", fileName);
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
	}

	// 原始的文件HTTPSERVLET下载
	@RequestMapping("downfile3")
	public String downLoadFile(Model model, HttpServletResponse response, String descFile) throws IOException {
		File file = new File("G:\\java\\eclipse_workspace\\yunhe\\springmvc\\file_updown_03\\src\\main\\webapp\\upload\\" + descFile);
		if (descFile == null || !file.exists()) {
			model.addAttribute("msg", "亲,您要下载的文件" + descFile + "不存在");
			return "load";
		}
		System.out.println(descFile);
		try {
			response.reset();
			// 设置ContentType
			response.setContentType("application/octet-stream; charset=utf-8");
			// 处理中文文件名中文乱码问题
			String fileName = new String(file.getName().getBytes("utf-8"), "ISO-8859-1");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			IOUtils.copy(new FileInputStream(file), response.getOutputStream());
			return null;
		} catch (Exception e) {
			model.addAttribute("msg", "下载失败");
			return "load";
		}
	}

	// 直接返回输出流,因为没有设置请求头所以不会显示
	@RequestMapping("/downfile")
	public StreamingResponseBody handle2() {
		return new StreamingResponseBody() {
			@Override
			public void writeTo(OutputStream outputStream) throws IOException {
				HttpHeaders headers = new HttpHeaders();
				FileUtils.copyFile(new File("G:\\java\\eclipse_workspace\\yunhe\\springmvc\\file_updown_03\\src\\main\\webapp\\upload\\1.png"), outputStream);
			}
		};
	}
	
	
	@RequestMapping("/cs01")
	public String cs01(HttpServletResponse response) throws IOException {
		return null;
	}
	
	@RequestMapping("/cs02")
	public void cs02(HttpServletResponse response) throws IOException {
		response.getWriter().print("cs02----123456");
	}

}
